package com.quang.covisoft.idiscovery.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.model.Comments;
import com.quang.covisoft.idiscovery.pref.Constants;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Covisoft on 3/27/17.
 */

public class CommentListAdapter extends ArrayAdapter<Comments> {

    private Activity activity;
    private int idLayout;
    private  List<Comments> listComm;

    public CommentListAdapter(Activity activity, int resource, List<Comments> objects) {
        super(activity, resource, objects);
        this.activity = activity;
        this.idLayout = resource;
        this.listComm = objects;
    }
    @SuppressLint("ViewHolder")
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = activity.getLayoutInflater();
        convertView = inflater.inflate(idLayout,parent, false);


        ImageView imgCommenter = (ImageView) convertView.findViewById(R.id.imgCommenter);
        TextView tvCommTitle = (TextView) convertView.findViewById(R.id.tvCommTitle);
        TextView tvCommentDesc = (TextView) convertView.findViewById(R.id.tvCommentDesc);

        String strUsFullname = new SqliteHelper(activity).getFullNamebyId(listComm.get(position).getReportByUserId());

        tvCommTitle.setText(strUsFullname);
        tvCommentDesc.setText(listComm.get(position).getReportDesc());

        return convertView;
    }


}
