package com.quang.covisoft.idiscovery.sqLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.quang.covisoft.idiscovery.model.Comments;
import com.quang.covisoft.idiscovery.model.EventObj;
import com.quang.covisoft.idiscovery.model.User;
import com.quang.covisoft.idiscovery.pref.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Covisoft on 3/10/17.
 */

public class SqliteHelper extends SQLiteOpenHelper {


    private static final String TAG = SqliteHelper.class.getSimpleName();

    // Script create table User
    public static String CREATE_TABLE_USER =
        "CREATE TABLE " + Constants.TABLE_USER + "(" +
            Constants.COLUME_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL" +
            "," + Constants.COLUME_EMAIL + " TEXT NOT NULL" +
            "," + Constants.COLUME_PWD + " TEXT NOT NULL" +
            "," + Constants.COLUME_FULLNAME + " TEXT NULL" +
            "," + Constants.COLUME_DOB + " TEXT NULL" +
                "," + Constants.COLUME_AVT + " BLOD " +
            ");";
    // Script create table Event
    public static String CREATE_TABLE_EVENT =
        "CREATE TABLE " + Constants.TABLE_EVENT + "(" +
            Constants.COLUME_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL" +
            ", " + Constants.COLUME_EVENTNAME + " TEXT NOT NULL" +
            "," + Constants.COLUME_EVENTLOCATION + " TEXT NOT NULL" +
            "," + Constants.COLUME_DESC + " TEXT " +
            "," + Constants.COLUME_DATE + " TEXT NOT NULL" +
                "," + Constants.COLUME_PHOTO + " BLOD NULL" +
            "," + Constants.COLUME_POST_BY + " INTEGER NOT NULL" +
            ", FOREIGN KEY (" + Constants.COLUME_POST_BY + ") REFERENCES " +
            Constants.TABLE_USER + "(" + Constants.COLUME_USER_ID + ")" +
            ");";
    // Script create table C
    public static String CREATE_TABLE_COMMENT =
        "CREATE TABLE " + Constants.TABLE_REPORT + "(" +
            Constants.COLUME_REPORT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL" +
            "," + Constants.COLUME_REPORT_DESC + " TEXT NOT NULL" +
            "," + Constants.COLUME_REF_USER_ID + " INTEGER NOT NULL" +
            "," + Constants.COLUME_REF_EVENT_ID + " INTEGER NOT NULL" +
            ", FOREIGN KEY (" + Constants.COLUME_REF_USER_ID + ") REFERENCES " +
            Constants.TABLE_USER + "(" + Constants.COLUME_USER_ID + ")" +
            ", FOREIGN KEY (" + Constants.COLUME_REF_EVENT_ID + ") REFERENCES " +
            Constants.TABLE_EVENT + "(" + Constants.COLUME_EVENT_ID + ")" +
            ");";


    public SqliteHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "SqliteHelper Oncreate.....");

        try {
            db.execSQL(CREATE_TABLE_USER);
            db.execSQL(CREATE_TABLE_EVENT);
            db.execSQL(CREATE_TABLE_COMMENT);
        } catch (Exception e) {
            Log.e("SQL Script", e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop table
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_EVENT);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_COMMENT);

        // Recreate
        onCreate(db);
    }

    // Check Login
    public int checkLogin(String userEmail, String userPwd) {
        SQLiteDatabase db = this.getReadableDatabase();
        int usrId = -1;
        try {
            String[] columns = {
                Constants.COLUME_USER_ID
            };
            // selection criteria
            String selection =
                Constants.COLUME_EMAIL + " = ?" + " AND " + Constants.COLUME_PWD + " = ?";
            // selection arguments
            String[] selectionArgs = {userEmail, userPwd};

            Cursor cursor = db.query(Constants.TABLE_USER,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
            }
            usrId = cursor.getInt(0);
            cursor.close();
            db.close();
        } catch (Exception e) {
            Log.e("SQL script", e.getMessage());
        }
        return usrId;
    }

    // Check Login
    public boolean checkEmail(String userEmail) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            String[] columns = {
                Constants.COLUME_USER_ID
            };
            // selection criteria
            String selection = Constants.COLUME_EMAIL + " = ?";
            // selection arguments
            String[] selectionArgs = {userEmail};

            Cursor cursor = db.query(Constants.TABLE_USER,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);
            int cursorCount = cursor.getCount();
            cursor.close();
            db.close();

            if (cursorCount > 0) {
                return true;
            }
        } catch (Exception e) {
            Log.e("SQL script", e.getMessage());
        }
        return false;
    }

    // Insert to User table
    public boolean Register(User objUser) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.COLUME_EMAIL, objUser.getEmail());
        values.put(Constants.COLUME_PWD, objUser.getPassword());
        values.put(Constants.COLUME_FULLNAME, objUser.getFullname());
        values.put(Constants.COLUME_DOB, objUser.getBirthday());
        values.put(Constants.COLUME_AVT, objUser.getAvatar());
        try {
            db.insert(Constants.TABLE_USER, null, values);
            db.close();
            return true;
        } catch (Exception e) {
            Log.e("SQL script", e.getMessage());
        }
        return false;
    }
    // Get Full name by ID
    public String getFullNamebyId(int usrId)
    {

        try{
            SQLiteDatabase db = this.getReadableDatabase();

            String queryGetUsrName = "select " + Constants.COLUME_FULLNAME + " from " + Constants.TABLE_USER + " where user_id = " + usrId;
            Cursor cursorUsName = db.rawQuery(queryGetUsrName,null);
            cursorUsName.moveToFirst();
            String strFullName = cursorUsName.getString(0);

            return strFullName;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    // Update to User table
    public boolean updateUserInfor(User objUser) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.COLUME_FULLNAME, objUser.getFullname());
        values.put(Constants.COLUME_DOB, objUser.getBirthday());
        values.put(Constants.COLUME_AVT, objUser.getAvatar());
        try {
            db.update(Constants.TABLE_USER, values, Constants.COLUME_USER_ID + " = ?",
                new String[]{String.valueOf(objUser.getUserId())});
            db.close();
            return true;
        } catch (Exception e) {
            Log.e("SQL script", e.getMessage());
        }
        return false;
    }
    // Update to Change password table
    public boolean updateUserPass(String strPWD, int usrId) {

        String query = "UPDATE "+ Constants.TABLE_USER + " SET password = " + strPWD +" where user_id = " +usrId;

        SQLiteDatabase db = this.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery(query,null);
            db.close();
            return true;
        } catch (Exception e) {
            Log.e("SQL script", e.getMessage());
        }
        return false;
    }
    // Load data from User table
    public User getUserInformation(int userId) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.query(Constants.TABLE_USER, null, Constants.COLUME_USER_ID + " = ?",
                new String[]{String.valueOf(userId)}, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            User dataUser = new User(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4),cursor.getBlob(5));
            return dataUser;

        } catch (Exception e) {
            Log.e("SQL script", e.getMessage());
        }
        return null;
    }

    // Insert to Event table
    public boolean AddnewEvents(EventObj objEvent) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.COLUME_EVENTNAME, objEvent.getEventName());
        values.put(Constants.COLUME_EVENTLOCATION, objEvent.getEventLocation());
        values.put(Constants.COLUME_DESC, objEvent.getEventDescription());
        values.put(Constants.COLUME_DATE, objEvent.getEventDate());
        values.put(Constants.COLUME_PHOTO, objEvent.getEventPhoto());
        values.put(Constants.COLUME_POST_BY, objEvent.getEventPostBy());
        try {
            db.insert(Constants.TABLE_EVENT, null, values);
            db.close();
            return true;
        } catch (Exception e) {
            Log.e("SQL Script", e.getMessage());
        }
        return false;
    }

    // Update to Event table
    public boolean updateEvents(EventObj objEvent) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.COLUME_EVENTNAME, objEvent.getEventName());
        values.put(Constants.COLUME_EVENTLOCATION, objEvent.getEventLocation());
        values.put(Constants.COLUME_DESC, objEvent.getEventDescription());
        values.put(Constants.COLUME_DATE, objEvent.getEventDate());
        values.put(Constants.COLUME_POST_BY, objEvent.getEventPostBy());
        try {
            db.update(Constants.TABLE_EVENT, values, Constants.COLUME_EVENT_ID + " = ?",
                new String[]{String.valueOf(objEvent.getEventId())});
            db.close();
            return true;
        } catch (Exception e) {
            Log.e("SQL Script", e.getMessage());
        }
        return false;
    }
    // Load my events
    public List<EventObj> getMyEventbyId(int usrId)
    {
        List<EventObj> modelList = new ArrayList<EventObj>();
        String query = "select * from "+ Constants.TABLE_EVENT + " where user_id = " +usrId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.moveToFirst()){
            do {
                EventObj model = new EventObj();
                model.setEventId(cursor.getInt(0));
                model.setEventName(cursor.getString(1));
                model.setEventLocation(cursor.getString(2));
                model.setEventDescription(cursor.getString(3));
                model.setEventDate(cursor.getString(4));
                model.setEventPhoto(cursor.getBlob(5));
                model.setEventPostBy(cursor.getInt(6));

                modelList.add(model);
            }while (cursor.moveToNext());
        }


        Log.d("Event by Id =", modelList.toString());

        return modelList;
    }
    // Load all of Events
    public List<EventObj> getAllEvent()
    {
        List<EventObj> modelList = new ArrayList<EventObj>();
        String query = "select * from "+ Constants.TABLE_EVENT;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.moveToFirst()){
            do {
                EventObj allEvModel = new EventObj();
                allEvModel.setEventId(cursor.getInt(0));
                allEvModel.setEventName(cursor.getString(1));
                allEvModel.setEventLocation(cursor.getString(2));
                allEvModel.setEventDescription(cursor.getString(3));
                allEvModel.setEventDate(cursor.getString(4));
                allEvModel.setEventPhoto(cursor.getBlob(5));
                allEvModel.setEventPostBy(cursor.getInt(6));

                modelList.add(allEvModel);
            }while (cursor.moveToNext());
        }


        Log.d("Event by Id =", modelList.toString());

        return modelList;
    }
    // Insert to Comment table
    public boolean postComment(Comments objComment) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.COLUME_REPORT_DESC, objComment.getReportDesc());
        values.put(Constants.COLUME_REF_USER_ID, objComment.getReportByUserId());
        values.put(Constants.COLUME_REF_EVENT_ID, objComment.getReportEventId());

        try {
            db.insert(Constants.TABLE_REPORT, null, values);
            db.close();
            return true;
        } catch (Exception e) {
            Log.e("SQL Script", e.getMessage());
        }
        return false;
    }
    public List<Comments> getCommentByEvId(int iEventId)
    {
        List<Comments> commentList = new ArrayList<Comments>();
        String query = "select * from "+ Constants.TABLE_REPORT + " where event_id = " +iEventId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.moveToFirst()){
            do {
                Comments cmModel = new Comments();
                cmModel.setReportDesc(cursor.getString(1));
                cmModel.setReportByUserId(cursor.getInt(2));

                commentList.add(cmModel);
            }while (cursor.moveToNext());
        }


        Log.d("Comment by Id =", commentList.toString());

        return commentList;
    }
}
