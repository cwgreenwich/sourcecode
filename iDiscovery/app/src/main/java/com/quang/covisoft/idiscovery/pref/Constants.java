package com.quang.covisoft.idiscovery.pref;

/**
 * Created by Covisoft on 3/10/17.
 */

public class Constants {

    public static final String TAG = "SQLite";


    // Database version
    public static final int DATABASE_VERSION = 3;


    // Database name.
    public static final String DATABASE_NAME = "iDiscovery";


    // Table User.
    public static final String TABLE_USER = "Users";
    public static final String COLUME_USER_ID = "user_id";
    public static final String COLUME_USERNAME = "username";
    public static final String COLUME_PWD = "password";
    public static final String COLUME_FULLNAME = "fullname";
    public static final String COLUME_EMAIL = "email";
    public static final String COLUME_DOB = "birthday";
    public static final String COLUME_AVT = "avatar";

    // Table Event
    public static final String TABLE_EVENT = "Events";
    public static final String COLUME_EVENT_ID = "event_id";
    public static final String COLUME_EVENTNAME = "eventname";
    public static final String COLUME_EVENTLOCATION = "location";
    public static final String COLUME_DESC = "desc";
    public static final String COLUME_DATE = "date";
    public static final String COLUME_PHOTO = "event_photo";
    public static final String COLUME_POST_BY = "user_id";


    // Table Report
    public static final String TABLE_REPORT = "Report";
    public static final String COLUME_REPORT_ID = "report_id";
    public static final String COLUME_REPORT_TITLE = "report_title";
    public static final String COLUME_REPORT_DESC = "desc";
    public static final String COLUME_REF_USER_ID = "user_id";
    public static final String COLUME_REF_EVENT_ID = "event_id";
    public static final long LOCATION_UPDATES_MIN_TIME = 1000;
    public static final float LOCATION_UPDATES_MIN_DISTANCE = 100;

    public static int RESULT_LOAD_IMAGE = 1;
    public static final int RESULT_PICK_LOCATION = 5969;

}
