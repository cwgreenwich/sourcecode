package com.quang.covisoft.idiscovery.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.activity.EventDetailActivity;
import com.quang.covisoft.idiscovery.model.EventObj;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;

import java.util.List;

/**
 * Created by Covisoft on 3/25/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.DataViewHolder> {

    static List<EventObj> evData;
    static Context context;

    public RecyclerAdapter(Context context,List<EventObj> evData) {
        this.evData = evData;
        this.context = context;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_item_event,parent,false);

        DataViewHolder viewHolder = new DataViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.tvItemEvName.setText(evData.get(position).getEventName());
        holder.tvItemPlace.setText(evData.get(position).getEventLocation());
        holder.tvItemDate.setText(evData.get(position).getEventDate());
        int usId = evData.get(position).getEventPostBy();
        String usrFullName = new SqliteHelper(context).getFullNamebyId(usId);
        holder.tvItemPostBy.setText("Posted by : "+ usrFullName);
        byte[] usrAvatar = evData.get(position).getEventPhoto();
        Bitmap bmp = BitmapFactory.decodeByteArray(evData.get(position).getEventPhoto(),0,usrAvatar.length);
        holder.imgItemEvent.setImageBitmap(bmp);

    }

    @Override
    public int getItemCount() {
        return evData == null ? 0 : evData.size();
    }

    public static class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvItemEvName;
        private ImageView imgItemEvent;
        private TextView tvItemPlace;
        private TextView tvItemDate;
        private TextView tvItemPostBy;

        public DataViewHolder(View itemView) {
            super(itemView);
            tvItemEvName = (TextView) itemView.findViewById(R.id.tvItemEventName);
            tvItemPlace = (TextView) itemView.findViewById(R.id.tvItemPlace);
            tvItemDate = (TextView) itemView.findViewById(R.id.tvItemDate);
            tvItemPostBy = (TextView) itemView.findViewById(R.id.tvItemPostBy);
            imgItemEvent = (ImageView) itemView.findViewById(R.id.imgItemEvent);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context,EventDetailActivity.class);

            Bundle extras = new Bundle();
            extras.putInt("item_index",getAdapterPosition());
            intent.putExtras(extras);
            context.startActivity(intent);
        }
    }
}
