package com.quang.covisoft.idiscovery.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.base.BaseMapActivity;

public class PickLocationActivity extends BaseMapActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_location);
    }
}
