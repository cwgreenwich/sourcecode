package com.quang.covisoft.idiscovery.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.base.BasePermissionActivity;
import com.quang.covisoft.idiscovery.model.EventObj;
import com.quang.covisoft.idiscovery.pref.Constants;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

import static com.quang.covisoft.idiscovery.pref.Constants.RESULT_PICK_LOCATION;

public class AddNewEventActivity extends BasePermissionActivity implements View.OnClickListener,
    OnDateSetListener {

    private ImageView imgEvent;

    private FloatingActionButton fabAddPhoto;
    private EditText edtEventName;
    private EditText edtEventDesc;
    private Button btnPickLocation;
    private EditText edtEventDate;
    SqliteHelper db;
    public int usId;
    private String picturePath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_event);
        loaduserDataLocal();
        db = new SqliteHelper(this);
        initComponents();
    }

    private void initComponents() {
        imgEvent = (ImageView) findViewById(R.id.imgEvent);

        fabAddPhoto = (FloatingActionButton) findViewById(R.id.fabAddPhoto);
        fabAddPhoto.setOnClickListener(this);

        edtEventName = (EditText) findViewById(R.id.edtEventName);
        edtEventDesc = (EditText) findViewById(R.id.edtEventDescription);
        btnPickLocation = (Button) findViewById(R.id.btnPickLocation);
        btnPickLocation.setOnClickListener(this);
        edtEventDate = (EditText) findViewById(R.id.edtDate);
        edtEventDate.setOnClickListener(this);

        Button btnAddnewEvent = (Button) findViewById(R.id.btnAddNewEvent);
        btnAddnewEvent.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabAddPhoto: {
                // check permission before select image
                requestRuntimePermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    "Message", new OnPermissionsLisnener() {
                        @Override
                        public void onPermissionGranted() {
                            Intent i = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, Constants.RESULT_LOAD_IMAGE);
                        }

                        @Override
                        public void onPermissionDenied() {
                            // TODO

                        }
                    });

                break;
            }
            case R.id.btnAddNewEvent: {
                validateForm();
                break;
            }
            case R.id.btnPickLocation: {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    startActivityForResult(builder.build(this), RESULT_PICK_LOCATION);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.edtDate: {
                showCalendar();
                hideSoftKeyboard(edtEventDate);
                break;
            }
        }
    }

    private void showCalendar() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        System.out.println("the selected " + mDay);

        DatePickerDialog dialog = new DatePickerDialog(this, this, mYear, mMonth, mDay);
        dialog.show();
    }

    private boolean validateForm() {
        //public EventObj(int eventId, String eventName, String eventLocation, String eventDescription, String eventDate, String eventPhoto, String eventPostBy) {

        String evName = edtEventName.getText().toString().trim();
        String evLocation = btnPickLocation.getText().toString().trim();
        String evDesc = edtEventDesc.getText().toString().trim();
        String evDate = edtEventDate.getText().toString().trim();

        if (evName.equalsIgnoreCase("")) {
            edtEventName.setError("Event name is not empty");
            return false;
        } else if (evDesc.equalsIgnoreCase("")) {
            edtEventDesc.setError("Description is not empty");
            return false;
        } else if (evLocation.equalsIgnoreCase("")) {
            btnPickLocation.setError("Location is not empty");
            return false;
        } else if (evDate.equalsIgnoreCase("")) {
            edtEventDate.setError("Date is not empty");
        } else if (picturePath == null) {
            Toast.makeText(this, "Please choose event image", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            edtEventName.setError(null);
            btnPickLocation.setError(null);
            edtEventDesc.setError(null);
            edtEventDate.setError(null);

            EventObj evData = new EventObj(0, evName, evLocation, evDesc, evDate,
                parseImg(picturePath),
                usId);

            try {
                if (db.AddnewEvents(evData)) {
                    Toast.makeText(this, "Create event successfully", Toast.LENGTH_LONG)
                        .show();
                    startActivity(new Intent(this, MainScreen.class));
                } else {
                    Toast
                        .makeText(this,
                            "Create event unsuccessful please check your input information",
                            Toast.LENGTH_LONG)
                        .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return true;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        int mYear = year;
        int mMonth = month;
        int mDay = dayOfMonth;
        edtEventDate.setText(new StringBuilder()
            // Month is 0 based so add 1
            .append(mMonth + 1).append("/").append(mDay).append("/")
            .append(mYear).append(" "));
    }

    protected void hideSoftKeyboard(EditText input) {
        input.setInputType(0);
        InputMethodManager imm = (InputMethodManager) getSystemService(
            Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    private void loaduserDataLocal() {
        SharedPreferences sharedPreferences = this
            .getSharedPreferences("userLocal", Context.MODE_PRIVATE);
        usId = sharedPreferences.getInt("userId", 0);
    }

    private byte[] parseImg(String sUrl) {
        try {
            FileInputStream fis = new FileInputStream(sUrl);
            byte[] image = new byte[fis.available()];
            fis.read(image);
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Google Places API
     * =====================================
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver()
                .query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
//            ImageView imageView = (ImageView) findViewById(R.id.imgEvent);
            imgEvent.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }

        if (requestCode == RESULT_PICK_LOCATION && resultCode == RESULT_OK
            && data != null) {
            btnPickLocation.setText(data.getDataString());
            btnPickLocation.setError(null);
        }

        if (requestCode == RESULT_PICK_LOCATION && data != null) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(context, data);
                btnPickLocation.setText(place.getName());
                btnPickLocation.setError(null);
            }
        }
    }

}
