package com.quang.covisoft.idiscovery.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.base.BasePermissionActivity;
import com.quang.covisoft.idiscovery.model.User;
import com.quang.covisoft.idiscovery.pref.Constants;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;
import com.quang.covisoft.idiscovery.utils.ValidateUtils;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

public class RegisterActivity extends BasePermissionActivity implements View.OnClickListener,
    View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {

    private EditText txtEmail;
    private EditText txtPass;
    private EditText txtConfirmPass;
    private EditText txtFullname;
    private EditText txtBirthday;

    private Button btnRegister;
    private CheckBox btnCheckAgree;

    private ImageView imgAvt;

    private SqliteHelper db;
    private String picturePath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        db = new SqliteHelper(this);
        initComponents();
    }

    private void initComponents() {
        txtEmail = (EditText) findViewById(R.id.edtEmail);
        txtPass = (EditText) findViewById(R.id.edtPass);
        txtConfirmPass = (EditText) findViewById(R.id.edtConfirm);
        txtFullname = (EditText) findViewById(R.id.edtFullname);
        txtBirthday = (EditText) findViewById(R.id.edtBirthday);
        txtBirthday.setOnFocusChangeListener(this);

        btnCheckAgree = (CheckBox) findViewById(R.id.chkTermOfService);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);

        imgAvt = (ImageView) findViewById(R.id.imgAvt);
        imgAvt.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister: {
                if (validateInput()) {
                    User usrData = new User();
                    usrData.setEmail(txtEmail.getText().toString().trim());
                    usrData.setPassword(txtPass.getText().toString().trim());
                    usrData.setFullname(txtFullname.getText().toString().trim());
                    usrData.setBirthday(txtBirthday.getText().toString().trim());
                    if (picturePath != null) {
                        byte[] arrAvat = parseImg(picturePath);
                        usrData.setAvatar(arrAvat);

                        if (db.Register(usrData)) {
                            Toast.makeText(this, "Register successfully", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(this, LoginActivity.class));
                            // finish this activity
                            finish();
                        } else {
                            Toast.makeText(this, "Create user unsuccessfully", Toast.LENGTH_LONG)
                                .show();
                        }
                    } else {
                        Toast.makeText(this, "Please choose your avatar", Toast.LENGTH_SHORT)
                            .show();
                    }
                }
                break;
            }
            case R.id.imgAvt: {
                // check permission before select image
                requestRuntimePermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    "Message", new BasePermissionActivity.OnPermissionsLisnener() {
                        @Override
                        public void onPermissionGranted() {
                            Intent i = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, Constants.RESULT_LOAD_IMAGE);
                        }

                        @Override
                        public void onPermissionDenied() {
                            // TODO

                        }
                    });
                break;
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver()
                .query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
//            ImageView imageView = (ImageView) findViewById(R.id.imgEvent);
            imgAvt.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edtBirthday: {
                hideSoftKeyboard(txtBirthday);
                showCalendar();
                break;
            }
        }
    }

    private boolean validateInput() {

        String email = txtEmail.getText().toString();
        String passWord = txtPass.getText().toString();
        String confirmPassword = txtConfirmPass.getText().toString();
        String fullName = txtFullname.getText().toString();
        String birthDay = txtBirthday.getText().toString();

        if (email.trim().equalsIgnoreCase("")) {
            txtEmail.setError("Please input your email");
            return false;
        } else if (!ValidateUtils.isEmailValid(email)) {
            txtEmail.setError("Invalid email");
            return false;
        } else if (passWord.trim().equalsIgnoreCase("")) {
            txtPass.setError("Please input your password");
            return false;
        } else if (confirmPassword.trim().equalsIgnoreCase("")) {
            txtConfirmPass.setError("Please input your confirm password");
            return false;
        } else if (!passWord.trim().equals(txtPass.getText().toString().trim())) {
            txtConfirmPass.setError("Your confirm password does not match with password");
            return false;
        } else if (fullName.trim().equalsIgnoreCase("")) {
            txtFullname.setError("Please input your full name");
            return false;
        } else if (!ValidateUtils.isFullNameValid(fullName)) {
            txtFullname.setError("Invalid full name");
            return false;
        } else if (birthDay.trim().equalsIgnoreCase("")) {
            txtBirthday.setError("Please input your birthday");
            return false;
        } else if (!btnCheckAgree.isChecked()) {
            btnCheckAgree.setError("Please check agree");
            return false;
        }

        txtEmail.setError(null);
        txtPass.setError(null);
        txtConfirmPass.setError(null);
        txtFullname.setError(null);
        txtBirthday.setError(null);
        btnCheckAgree.setError(null);

        return true;
    }

    private void clearAllField() {
        txtEmail.setText("");
        txtPass.setText("");
        txtFullname.setText("");
        txtBirthday.setText("");

        btnCheckAgree.setChecked(false);
    }

    protected void hideSoftKeyboard(EditText input) {
        input.setInputType(0);
        InputMethodManager imm = (InputMethodManager) getSystemService(
            Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    private void showCalendar() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        System.out.println("the selected " + mDay);

        DatePickerDialog dialog = new DatePickerDialog(this, this, mYear, mMonth, mDay);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        int mYear = year;
        int mMonth = monthOfYear;
        int mDay = dayOfMonth;
        txtBirthday.setText(new StringBuilder()
            // Month is 0 based so add 1
            .append(mMonth + 1).append("/").append(mDay).append("/")
            .append(mYear).append(" "));
    }

    private byte[] parseImg(String sUrl) {
        try {
            FileInputStream fis = new FileInputStream(sUrl);
            byte[] image = new byte[fis.available()];
            fis.read(image);
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
