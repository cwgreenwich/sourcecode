package com.quang.covisoft.idiscovery.model;

/**
 * Created by Covisoft on 3/10/17.
 */

public class User {
    private int userId;
    private String email;
    private String password;
    private String fullname;
    private String birthday;
    private byte[] avatar;
    public User(int userId, String email, String password, String fullname, String birthday,byte[] avatar) {
        this.userId = userId;
        this.email = email;
        this.password = password;
        this.fullname = fullname;
        this.birthday = birthday;
        this.avatar = avatar;
    }


    public User() {
    }

    public int getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFullname() {
        return fullname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }
}
