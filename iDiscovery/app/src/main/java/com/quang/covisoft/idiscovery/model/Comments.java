package com.quang.covisoft.idiscovery.model;

/**
 * Created by Covisoft on 3/20/17.
 */

public class Comments {
    private int reportId;
    private String reportDesc;
    private int reportByUserId;
    private int reportEventId;

    public Comments() {
    }

    public Comments(int reportId, String reportDesc, int reportByUserId, int reportEventId) {
        this.reportId = reportId;
        this.reportDesc = reportDesc;
        this.reportByUserId = reportByUserId;
        this.reportEventId = reportEventId;
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    public String getReportDesc() {
        return reportDesc;
    }

    public void setReportDesc(String reportDesc) {
        this.reportDesc = reportDesc;
    }

    public int getReportByUserId() {
        return reportByUserId;
    }

    public void setReportByUserId(int reportByUserId) {
        this.reportByUserId = reportByUserId;
    }

    public int getReportEventId() {
        return reportEventId;
    }

    public void setReportEventId(int reportEventId) {
        this.reportEventId = reportEventId;
    }
}
