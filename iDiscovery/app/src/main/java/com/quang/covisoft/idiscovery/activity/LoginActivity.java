package com.quang.covisoft.idiscovery.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;
import com.quang.covisoft.idiscovery.utils.ValidateUtils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private Button btnRegister;
    private Button btnForgotPass;

    private EditText txtUserEmail;
    private EditText txtPwd;
    private SqliteHelper sql;

    private int usrId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sql = new SqliteHelper(this);
        sql.getWritableDatabase();

        iniViewComponents();
    }

    private void iniViewComponents() {
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        btnRegister = (Button) findViewById(R.id.btnGoRegister);
        btnRegister.setOnClickListener(this);

        btnForgotPass = (Button) findViewById(R.id.btnForgotPassword);
        btnForgotPass.setOnClickListener(this);

        txtUserEmail = (EditText) findViewById(R.id.edtusrEmail);

        txtPwd = (EditText) findViewById(R.id.edtPassword);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (checkLogin(txtUserEmail.getText().toString(), txtPwd.getText().toString())) {
                    Toast.makeText(this, "Login successfully", Toast.LENGTH_LONG).show();
                    saveLoginInfor(usrId);
                    Intent intent = new Intent(this, MainScreen.class);
//                    intent.putExtra("usID", usrId);
                    startActivity(intent);
                    // finish this activity
                    finish();
                }
                break;
            case R.id.btnGoRegister:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
            case R.id.btnForgotPassword:
                showForgotPasswordDialog();
                break;
        }
    }

    private boolean checkLogin(String usrEmail, String usrPwd) {
        if (sql.checkEmail(txtUserEmail.getText().toString())) {
            usrId = sql.checkLogin(txtUserEmail.getText().toString(), txtPwd.getText().toString());
            if (usrId != -1) {
                return true;
            } else if (usrEmail.trim().equalsIgnoreCase("")) {
                txtUserEmail.setError("Email is not empty");
                return false;
            } else if (!ValidateUtils.isEmailValid(usrEmail)) {
                txtUserEmail.setError("Invalid email");
                return false;
            } else if (usrPwd.trim().equalsIgnoreCase("")) {
                txtPwd.setError("Password is not empty");
                return false;
            } else {
                return true;
            }
        } else {
            Toast.makeText(this, "User is not exist please register an account", Toast.LENGTH_LONG)
                .show();
        }
        return false;
    }

    private void emptyAllField() {
        txtUserEmail.setText("");
        txtPwd.setText("");
    }

    private void showForgotPasswordDialog() {
        // get view forgot password
        View forgotPasswordView = LayoutInflater.from(this)
            .inflate(R.layout.dialog_fotgot_password,
                (ViewGroup) findViewById(R.id.dialog_forgot_password));
        // get UI elements
        final EditText edtNewPassword = (EditText) forgotPasswordView
            .findViewById(R.id.edtNewPassword);

        // create & assign view forgot password for dialog
        final AlertDialog forgotPasswordDialog = new AlertDialog.Builder(this)
            .setTitle(null)
            .setView(forgotPasswordView)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok, null)
            .create();
        forgotPasswordDialog.setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                forgotPasswordDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(
                    new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // cancel button clicked
                            // dismiss dialog
                            forgotPasswordDialog.dismiss();
                        }
                    });
                forgotPasswordDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
                    new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // ok button

                            String emailRecover = edtNewPassword.getText().toString();
                            if (!emailRecover.equals("")) {
                                sendForgotPass();
                                forgotPasswordDialog.dismiss();
                            } else {
                                edtNewPassword.setError("Please input your email");
                            }


                        }
                    });
            }
        });
        // show dialog
        forgotPasswordDialog.show();
    }

    private void saveLoginInfor(int userId) {

        SharedPreferences sharedPreference = this
            .getSharedPreferences("userLocal", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreference.edit();

        editor.putInt("userId", userId);
        editor.apply();
    }

    protected void sendEmail() {
        Log.i("Send email", "");

        String[] TO = {"someone@gmail.com"};
        String[] CC = {"xyz@gmail.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendForgotPass() {
        Toast.makeText(this, "Send email successfully.", Toast.LENGTH_SHORT).show();
    }
}
