package com.quang.covisoft.idiscovery.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.base.BasePermissionActivity;
import com.quang.covisoft.idiscovery.model.User;
import com.quang.covisoft.idiscovery.pref.Constants;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;
import com.quang.covisoft.idiscovery.utils.ValidateUtils;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

public class AccountInfoActivity extends BasePermissionActivity implements View.OnClickListener,
    View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {

    private ImageView imgAvatar;
    private FloatingActionButton btnChangeAvatar;

    private EditText edtFullname;
    private EditText edtEmail;
    private EditText edtBirthday;

    private Button btnUpdateInfo;
    private Button btnChangePass;
    private Button btnSaveUpdate;
    public int usId;
    private String picturePath;
    SqliteHelper db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_info);
        loaduserDataLocal();
        db = new SqliteHelper(this);
        initComponents();
    }

    private void initComponents() {
        imgAvatar = (ImageView) this.findViewById(R.id.imgInforAvatar);
        btnChangeAvatar = (FloatingActionButton) this.findViewById(R.id.btnChangeAvatar);
        btnChangeAvatar.setOnClickListener(this);

        edtFullname = (EditText) this.findViewById(R.id.edtinforFullname);
        edtEmail = (EditText) this.findViewById(R.id.edtInfoEmail);
        edtBirthday = (EditText) this.findViewById(R.id.edtInforBirthday);
        edtBirthday.setOnFocusChangeListener(this);

        btnUpdateInfo = (Button) findViewById(R.id.btnUpdateInfo);
        btnUpdateInfo.setOnClickListener(this);

        btnChangePass = (Button) findViewById(R.id.btnChangePass);
        btnChangePass.setOnClickListener(this);

        btnSaveUpdate = (Button) findViewById(R.id.btnSaveUpdate);
        btnSaveUpdate.setOnClickListener(this);

        btnChangeAvatar.setVisibility(View.GONE);
        btnChangePass.setVisibility(View.GONE);
        btnSaveUpdate.setVisibility(View.GONE);

        edtFullname.setEnabled(false);
        edtBirthday.setEnabled(false);
        edtEmail.setEnabled(false);

        loadUserInformation();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpdateInfo: {
                updateViewStates(true);
                break;
            }
            case R.id.btnChangePass: {
                showPopupChangePassword();
                break;

            }
            case R.id.btnChangeAvatar: {
                // check permission before select image
                requestRuntimePermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    "Message", new BasePermissionActivity.OnPermissionsLisnener() {
                        @Override
                        public void onPermissionGranted() {
                            Intent i = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, Constants.RESULT_LOAD_IMAGE);
                        }

                        @Override
                        public void onPermissionDenied() {
                            // TODO

                        }
                    });
                break;
            }
            case R.id.btnSaveUpdate: {
                updateUserInformation();
                break;
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver()
                .query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
//            ImageView imageView = (ImageView) findViewById(R.id.imgEvent);
            imgAvatar.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    private void showCalendar() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        System.out.println("the selected " + mDay);

        DatePickerDialog dialog = new DatePickerDialog(this, this, mYear, mMonth, mDay);
        dialog.show();
    }

    protected void hideSoftKeyboard(EditText input) {
        input.setInputType(0);
        InputMethodManager imm = (InputMethodManager) getSystemService(
            Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    private void loaduserDataLocal() {
        SharedPreferences sharedPreferences = this
            .getSharedPreferences("userLocal", Context.MODE_PRIVATE);
        usId = sharedPreferences.getInt("userId", 0);
    }

    private byte[] parseImg(String sUrl) {
        try {
            FileInputStream fis = new FileInputStream(sUrl);
            byte[] image = new byte[fis.available()];
            fis.read(image);
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        int mYear = year;
        int mMonth = month;
        int mDay = dayOfMonth;
        edtBirthday.setText(new StringBuilder()
            // Month is 0 based so add 1
            .append(mMonth + 1).append("/").append(mDay).append("/")
            .append(mYear).append(" "));
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edtInforBirthday: {
                hideSoftKeyboard(edtBirthday);
                showCalendar();
                break;
            }
        }
    }

    private void updateViewStates(boolean isState) {
        if (isState) {
            btnUpdateInfo.setVisibility(View.GONE);
            btnChangeAvatar.setVisibility(View.VISIBLE);
            btnChangePass.setVisibility(View.VISIBLE);
            btnSaveUpdate.setVisibility(View.VISIBLE);

            edtFullname.setEnabled(true);
            edtBirthday.setEnabled(true);

        } else {
            btnUpdateInfo.setVisibility(View.VISIBLE);
            btnChangeAvatar.setVisibility(View.GONE);
            btnChangePass.setVisibility(View.GONE);
            btnSaveUpdate.setVisibility(View.GONE);

            edtFullname.setEnabled(false);
            edtBirthday.setEnabled(false);
        }
    }

    private void loadUserInformation() {
        try {

            User objUser = db.getUserInformation(usId);
            edtFullname.setText(objUser.getFullname().toString());
            edtEmail.setText(objUser.getEmail().toString());
            edtBirthday.setText(objUser.getBirthday().toString());
            byte[] usrAvatar = objUser.getAvatar();
            if (usrAvatar != null) {
                Bitmap bmp = BitmapFactory
                    .decodeByteArray(objUser.getAvatar(), 0, usrAvatar.length);
                imgAvatar.setImageBitmap(bmp);
            } else {
                imgAvatar.setImageResource(R.drawable.ic_account_circle_black_24dp);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPopupChangePassword() {
        // get view forgot password
        View forgotPasswordView = LayoutInflater.from(this)
            .inflate(R.layout.dialog_change_password,
                (ViewGroup) findViewById(R.id.dialog_change_password));
        // get UI elements
        final EditText edtChangeNewPassword = (EditText) forgotPasswordView
            .findViewById(R.id.edtChangeNewPass);
        final EditText edtChangeConfirmPassword = (EditText) forgotPasswordView
            .findViewById(R.id.edtChangeConfirmPass);

        // create & assign view forgot password for dialog
        final AlertDialog changePasswordDialog = new AlertDialog.Builder(this)
            .setTitle(null)
            .setView(forgotPasswordView)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok, null)
            .create();
        changePasswordDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                changePasswordDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // cancel button clicked
                            // dismiss dialog
                            changePasswordDialog.dismiss();
                        }
                    });
                changePasswordDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // ok button
                            // update new password to your db & dissmis dialog
                            boolean isCheck = true;
                            String newPassword = edtChangeNewPassword.getText().toString();
                            String confirmPassword = edtChangeConfirmPassword.getText().toString();

                            if (newPassword.equals("")) {
                                isCheck = false;
                                edtChangeNewPassword.setError("Please input new password");
                            }
                            if (confirmPassword.equals("")) {
                                isCheck = false;
                                edtChangeConfirmPassword.setError("Please input confirm password");
                            }
                            if (!newPassword.equals(confirmPassword)) {
                                isCheck = false;
                                edtChangeConfirmPassword
                                    .setError("New password not match with confirm password");
                            }
                            if (isCheck) {

                                updatePassword(newPassword);
                                changePasswordDialog.dismiss();

                            }

                        }
                    });
            }
        });
        // show dialog
        changePasswordDialog.show();
    }

    private void updateUserInformation() {
        String strFullname = edtFullname.getText().toString().trim();
        String strBirtday = edtBirthday.getText().toString().trim();

        if (checkValidate()) {
            User objUser = new User();
            objUser.setFullname(strFullname);
            objUser.setBirthday(strBirtday);
            objUser.setUserId(usId);
            if (picturePath != null) {
                objUser.setAvatar(parseImg(picturePath));
            }

            if (db.updateUserInfor(objUser)) {
                updateViewStates(false);
                loadUserInformation();
                Toast.makeText(this, "Update information successfully", Toast.LENGTH_LONG).show();
            }
        }

    }

    private boolean checkValidate() {
        String fullName = edtFullname.getText().toString().trim();
        String email = edtEmail.getText().toString().trim();
        String birthDay = edtBirthday.getText().toString().trim();

        if (fullName.equals("")) {
            edtFullname.setError("Please input your full name");
            return false;
        } else if (!ValidateUtils.isFullNameValid(fullName)) {
            edtFullname.setError("Invalid full name");
            return false;
        } else if (email.equalsIgnoreCase("")) {
            edtEmail.setError("Please input your email");
            return false;
        } else if (!ValidateUtils.isEmailValid(email)) {
            edtEmail.setError("Invalid email");
            return false;
        } else if (birthDay.equals("")) {
            edtBirthday.setError("Please input your birthday");
            return false;
        } else {
            edtFullname.setError(null);
            edtEmail.setError(null);
            edtBirthday.setError(null);
        }

        return true;
    }

    private void updatePassword(String strNewPass) {
        if (db.updateUserPass(strNewPass, usId)) {
            updateViewStates(false);
            Toast.makeText(this, "Update password successfully", Toast.LENGTH_LONG).show();
        }
    }
}
