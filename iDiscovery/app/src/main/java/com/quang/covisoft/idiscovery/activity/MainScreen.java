package com.quang.covisoft.idiscovery.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.model.User;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;

public class MainScreen extends AppCompatActivity implements OnClickListener {

    private View accountInfo;
    private View myEvents, anotherEvents;
    private TextView tvFullName;
    private TextView tvEmail;
    private FloatingActionButton fabAddEvent;
    private ImageView imgMainAvt;

    public int usId;
    private SqliteHelper db;
    public User usrData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

//        Bundle bundle = getIntent().getExtras();
//        usId = bundle.getInt("usID", -1);
        loaduserDataLocal();
        initComponents();
        db = new SqliteHelper(this);
        getUserInformation();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.accountInfo:
                startActivity(new Intent(this, AccountInfoActivity.class));
                break;
            case R.id.myEvents:
                startActivity(new Intent(this, MyEventsActivity.class));
                break;
            case R.id.anotherEvents:
                startActivity(new Intent(this, AnotherEventsActivity.class));
                break;
            case R.id.fabAddEvent:
                startActivity(new Intent(this, AddNewEventActivity.class));
                break;
        }
    }

    private void initComponents() {
        accountInfo = findViewById(R.id.accountInfo);
        myEvents = findViewById(R.id.myEvents);
        anotherEvents = findViewById(R.id.anotherEvents);
        fabAddEvent = (FloatingActionButton) findViewById(R.id.fabAddEvent);

        // set onClick
        accountInfo.setOnClickListener(this);
        myEvents.setOnClickListener(this);
        anotherEvents.setOnClickListener(this);
        fabAddEvent.setOnClickListener(this);

        tvFullName = (TextView) findViewById(R.id.tvFullname);
        tvEmail = (TextView) findViewById(R.id.tvEmail);

        imgMainAvt = (ImageView) findViewById(R.id.imgMainAvt);

    }

    private void getUserInformation() {

        try {
            usrData = db.getUserInformation(usId);
            tvFullName.setText(usrData.getFullname());
            tvEmail.setText(usrData.getEmail());
            byte[] usrAvatar = usrData.getAvatar();
            if(usrAvatar != null)
            {
                Bitmap bmp = BitmapFactory.decodeByteArray(usrData.getAvatar(),0,usrAvatar.length);
                imgMainAvt.setImageBitmap(bmp);
            }else
            {
                imgMainAvt.setImageResource(R.drawable.ic_account_circle_black_24dp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void loaduserDataLocal()
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences("userLocal", Context.MODE_PRIVATE);
        usId = sharedPreferences.getInt("userId", 0);
    }
}
