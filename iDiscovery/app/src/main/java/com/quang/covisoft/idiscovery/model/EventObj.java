package com.quang.covisoft.idiscovery.model;

/**
 * Created by Covisoft on 3/20/17.
 */

public class EventObj {
    private int eventId;
    private String eventName;
    private String eventLocation;
    private String eventDescription;
    private String eventDate;
    private byte[] eventPhoto;
    private int eventPostBy;

    public EventObj() {
    }

    public EventObj(int eventId, String eventName, String eventLocation, String eventDescription, String eventDate, byte[] eventPhoto, int eventPostBy) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventLocation = eventLocation;
        this.eventDescription = eventDescription;
        this.eventDate = eventDate;
        this.eventPhoto = eventPhoto;
        this.eventPostBy = eventPostBy;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public byte[] getEventPhoto() {
        return eventPhoto;
    }

    public void setEventPhoto(byte[] eventPhoto) {
        this.eventPhoto = eventPhoto;
    }

    public int getEventPostBy() {
        return eventPostBy;
    }

    public void setEventPostBy(int eventPostBy) {
        this.eventPostBy = eventPostBy;
    }
}