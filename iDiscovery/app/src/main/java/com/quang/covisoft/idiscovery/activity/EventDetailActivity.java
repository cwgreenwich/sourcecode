package com.quang.covisoft.idiscovery.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.adapter.CommentListAdapter;
import com.quang.covisoft.idiscovery.model.Comments;
import com.quang.covisoft.idiscovery.model.EventObj;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;

import java.util.ArrayList;
import java.util.List;

public class EventDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private SqliteHelper db;
    private List<EventObj> evData;
    private int position;

    private ImageView imgDetailEvent;
    private TextView tvDetailPlace , tvDetailDate, tvDetailPostedBy, tvEvDescr;

    private CommentListAdapter comListAdapter;

    ListView listComment;
    private List<Comments> list;

    private EditText edtComment;
    private ImageButton btnSend;
    public int usId, eventId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        db = new SqliteHelper(this);
        loaduserDataLocal();
        loadEventDetail();
        edtComment = (EditText)findViewById(R.id.edtComment);
        btnSend = (ImageButton)findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

    }

    private void loadEventDetail()
    {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        imgDetailEvent = (ImageView) findViewById(R.id.imgDetailEvent);
        tvDetailPlace = (TextView) findViewById(R.id.tvDetailPlace);
        tvDetailDate = (TextView) findViewById(R.id.tvDetailDate);
        tvDetailPostedBy = (TextView) findViewById(R.id.tvDetailPostedBy);
        tvEvDescr = (TextView) findViewById(R.id.tvDescription);

        position = bundle.getInt("item_index");
        evData = db.getAllEvent();
        if (evData.size() > 0)
        {
            tvDetailDate.setText(evData.get(position).getEventDate());
            tvDetailPlace.setText(evData.get(position).getEventLocation());
            //tvDetailPostedBy.setText("Posted by : " + evData.get(position).getEventLocation());
            eventId = evData.get(position).getEventId();
            tvEvDescr.setText(evData.get(position).getEventDescription());
            byte[] usrAvatar = evData.get(position).getEventPhoto();
            Bitmap bmp = BitmapFactory.decodeByteArray(evData.get(position).getEventPhoto(),0,usrAvatar.length);
            imgDetailEvent.setImageBitmap(bmp);
            loadComment(eventId);
        }
    }
    private void loadComment(int evId)
    {
        listComment = (ListView) findViewById(R.id.lvComments);
        list = db.getCommentByEvId(evId);
        comListAdapter = new CommentListAdapter(this,R.layout.list_item_comment, list);
        listComment.setAdapter(comListAdapter);
        setListViewHeightBasedOnChildren(listComment);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnSend :
            {
                if(!edtComment.getText().toString().trim().equals(""))
                {
                    Comments objCmt = new Comments();
                    objCmt.setReportByUserId(usId);
                    objCmt.setReportEventId(eventId);
                    objCmt.setReportDesc(edtComment.getText().toString().trim());
                    db.postComment(objCmt);

                    Toast.makeText(this, "Post comment successfully", Toast.LENGTH_SHORT).show();
                    edtComment.setText("");
                    hideSoftKeyboard(edtComment);
                    comListAdapter.notifyDataSetChanged();
                    loadComment(eventId);
                }else
                {
                    edtComment.setError("Please type your comment");
                }

                break;
            }
        }

    }
    private void loaduserDataLocal()
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences("userLocal", Context.MODE_PRIVATE);
        usId = sharedPreferences.getInt("userId", 0);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    protected void hideSoftKeyboard(EditText input) {
        input.setInputType(0);
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }
}
