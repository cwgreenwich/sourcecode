package com.quang.covisoft.idiscovery.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.quang.covisoft.idiscovery.R;
import com.quang.covisoft.idiscovery.adapter.RecyclerAdapter;
import com.quang.covisoft.idiscovery.model.EventObj;
import com.quang.covisoft.idiscovery.sqLite.SqliteHelper;

import java.util.ArrayList;
import java.util.List;

public class MyEventsActivity extends AppCompatActivity {

    private SqliteHelper db;
    private List<EventObj> evData;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public int usId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_events);
        loaduserDataLocal();
        db = new SqliteHelper(this);
        evData = new ArrayList<EventObj>();
        evData = db.getMyEventbyId(usId);

        mRecyclerView = (RecyclerView) findViewById(R.id.rvMyEvents);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecyclerAdapter(this,evData);
        mRecyclerView.setAdapter(mAdapter);
    }
    private void loaduserDataLocal()
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences("userLocal", Context.MODE_PRIVATE);
        usId = sharedPreferences.getInt("userId", 0);
    }
}
